require 'test_helper'

class StationTest < ActiveSupport::TestCase
  test "Station names can not be empty" do
    # Test a standard name
    s = Station.create(:name => "Boucherville")
    assert s.valid?

    # Make sure we can not create an empty station
    s = Station.create(:name => nil)
    assert !s.valid?

    # Make sure we can not create a station name which starts with a digit
    s = Station.create(:name => "5Boucherville")
    assert !s.valid?
  end
end
