class StationsController < ApplicationController

  def index
    @stations = Station.all
    respond_to do |format|
      format.html
      format.json { render :json => @stations }
    end
  end

  def new
    @station = Station.new
  end

  def edit
    @station = Station.find(params[:id])
  end

  def create
    @station = Station.new(params[:station])

    if @station.save
      redirect_to stations_path, :notice => 'Station was successfully created.'
    else
      render :action => "new"
    end
  end

  def update
    @station = Station.find(params[:id])

    if @station.update_attributes(params[:station])
      redirect_to stations_path, :notice => 'Station was successfully updated.'
    else
      render :action => "edit"
    end
  end

  def destroy
    @station = Station.destroy(params[:id])
    redirect_to stations_path, :notice => 'Station was successfully deleted.'
  end

end
